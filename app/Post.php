<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
	protected $fillable = ['author_id', 'title', 'body', 'slug', 'active'];

    public function post()
    {
    	return $this->belongsTo('App\User');
    }

    public function getTitle()
    {
    	return $this->title;
    }
    
}
