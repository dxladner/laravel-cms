<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\PostFormRequest;
use App\Http\Requests;
use App\Player;
use Auth;
use App\Lineup;
use App\Http\Requests\LineupFormRequest;



class ContestantsController extends Controller
{
    public function index()
    {
        $quarterbacks = Player::where('position', 'quarterback')->get();
        $runningbacks_one = Player::where('position', '=', 'runningback_one')->get();
        $runningbacks_two = Player::where('position', '=', 'runningback_two')->get();
        $widereceivers_one = Player::where('position', '=', 'widereceiver_one')->get();
        $widereceivers_two = Player::where('position', '=', 'widereceiver_two')->get();
        $tightends = Player::where('position', '=', 'tightend')->get();
        $defenses = Player::where('position', 'defense')->get();

        return view('contestants/dashboard', compact('quarterbacks', 'runningbacks_one', 'runningbacks_two', 'widereceivers_one', 'widereceivers_two', 'tightends','defenses'));
    }

    public function newLineup(LineupFormRequest $request)
    {
        $weekly_lineup = new Lineup(array(
            'week'              => $request->get( 'week' ),
            'user_id'   		=> $request->get( 'user_id' ),
            'quarterback'   	=> $request->get( 'quarterback' ),
            'runningback_one'	=> $request->get( 'runningback_one' ),
            'runningback_two'	=> $request->get( 'runningback_two' ),
            'widereceiver_one'	=> $request->get( 'widereceiver_one' ),
            'widereceiver_two'  => $request->get( 'widereceiver_two' ),
            'tightend'          => $request->get( 'tightend' ),
            'defense'           => $request->get( 'defense' )

        ));

        $weekly_lineup->save();

        return redirect()->back()->with('status', 'Your lineup has been created!');
    }
}
