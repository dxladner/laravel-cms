<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\PlayerFormRequest;
use App\Player;


class PlayersController extends Controller
{
    public function index()
    {
    	$quarterbacks = Player::where('position', '=', 'quarterback')->get();
    	$runningbacks_one = Player::where('position', '=', 'runningback_one')->get();
        $runningbacks_two = Player::where('position', '=', 'runningback_two')->get();
    	$widereceivers_one = Player::where('position', '=', 'widereceiver_one')->get();
        $widereceivers_two = Player::where('position', '=', 'widereceiver_two')->get();
    	$tightends = Player::where('position', '=', 'tightend')->get();
    	$defenses = Player::where('position', '=', 'defense')->get();

    	return view('cms/players', compact('quarterbacks', 'runningbacks_one', 'runningbacks_two', 'widereceivers_one', 'widereceivers_two', 'tightends', 'defenses'));
    }

    public function newPlayer(PlayerFormRequest $request)
    {
        $player = new Player(array(
            'name'   	      => $request->get('name'),
            'team'   	      => $request->get('team'),
            'position'	      => $request->get('position')
        ));

        $player->save();

        return redirect()->back()->with('status', 'Your player has been created!');
    }

    public function edit($id)
    {
        $player = Player::findOrFail($id);
        return view('cms/playeredit', compact('player'));
    }

    public function update($id, PlayerFormRequest $request)
    {
        $player = Player::findOrFail($id);
        $player->name = $request->get('name');
        $player->team = $request->get('team');
        $player->position = $request->get('position');
        
        $player->save();
        return redirect('cms/players')->with('status', 'The player '.$player->name.' has been updated!');
    }

    public function destroy($id)
    {
        $player = Player::findOrFail($id);
        $player->delete();
        return redirect('cms/players')->with('status', 'The player '.$player->name.' has been deleted!');
    }

}
