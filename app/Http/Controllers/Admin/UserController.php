<?php

namespace App\Http\Controllers\Admin;

use App\user;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\CanResetPassword;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function index()
    {
    	$users = User::orderBy('created_at', 'desc')->get();
    	return view('admin/user/index')->with('users', $users);
    }

    public function show($id)
    {
        $user = User::findOrFail($id);
        return view('admin/user/show', compact('user'));
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('admin/user/edit', compact('user'));
    }

    public function update($id, Request $request)
    {
        $user = User::findOrFail($id);
        $user->name = $request->get('name');
        $user->email = $request->get('email');

        $user->save();
        return view('admin/user/show', compact('user'))->with('status', 'Your profile has been updated!');
    }
   
}
