<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class LineupFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
            'week'              => 'required|min:1',
            'user_id'           => 'required|min:1',
            'quarterback'       => 'required|min:1',
            'runningback_one'   => 'required|min:1',
            'runningback_two'   => 'required|min:1',
            'widereceiver_one'  => 'required|min:1',
            'widereceiver_two'  => 'required|min:1',
            'tightend'          => 'required|min:1',
            'defense'           => 'required|min:1'
        ];
    }
}
