<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('home', 'PagesController@home');

Route::get('/', 'PagesController@home');
Route::get('/about', 'PagesController@about');
Route::get('/contact', 'PagesController@contact');
Route::get('/welcome', 'PagesController@welcome');

// Ticket Routes
Route::get('/create', 'TicketsController@create');
Route::post('/create', 'TicketsController@store');
Route::get('/tickets', 'TicketsController@index');
Route::get('/ticket/{slug?}', 'TicketsController@show');
Route::get('ticket/{slug?}/edit', 'TicketsController@edit');
Route::post('/ticket/{slug?}/edit', 'TicketsController@update');
Route::post('/ticket/{slug?}/delete', 'TicketsController@destroy');

// Comment Routes
Route::post('/comment', 'CommentsController@newComment');

// Users Routes 
Route::get('users/register', 'Auth\AuthController@getRegister');
Route::post('users/register', 'Auth\AuthController@postRegister');

Route::get('users/login', 'Auth\AuthController@getLogin');
Route::post('users/login', 'Auth\AuthController@postLogin');

Route::get('users/logout', 'Auth\AuthController@logout');


// CMS
Route::get('/cms', 'CMS\CMSController@index');

// Contestants
Route::get('/contestants', 'ContestantsController@index');
Route::post('/contestants','ContestantsController@newLineup');

// Players 
Route::get('/cms/players', 'CMS\PlayersController@index');
Route::post('/cms/players', 'CMS\PlayersController@newPlayer');
Route::get('/cms/player/{id?}/edit', 'CMS\PlayersController@edit');
Route::post('/cms/player/{id?}/edit', 'CMS\PlayersController@update');
Route::post('/cms/player/{id?}/delete', 'CMS\PlayersController@destroy');


// Admin
Route::group(['prefix' => 'admin', 'namespace' => 'admin'], function()
{
	Route::resource('/user', 'UserController');
	Route::patch('/user/{id?}/edit', 'UserController@update');
	
});

// redirect route 
Route::any('/userdeny', array('as' => 'userdeny', 'uses' => 'PagesController@welcome'));

Route::group(['middleware' => 'web'], function () {
    Route::auth();

    Route::get('/home', 'HomeController@index');
});

// Posts 
Route::post('/posts','PostsController@newPost');
Route::get('/posts','PostsController@index');
Route::get('/posts/{id?}', 'PostsController@show');
Route::get('/posts/edit/{id}', 'PostsController@edit');
Route::post('/posts/edit/{id}', 'PostsController@update');
Route::post('/posts/{id?}/delete', 'PostsController@destroy');
