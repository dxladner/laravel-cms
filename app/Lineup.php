<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lineup extends Model
{
    protected $fillable = [ 'week', 'user_id', 'quarterback', 'runningback_one', 'runningback_two', 'widereceiver_one', 'widereceiver_two', 'tightend', 'defense' ];

    public function lineup()
    {
    	return $this->belongsTo('App\Contestant');
    }
}
