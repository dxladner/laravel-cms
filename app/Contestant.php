<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contestant extends Model
{
    public function lineups()
	{
	    return $this->hasMany('App\Lineup', 'user_id');
	}
}
