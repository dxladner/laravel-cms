<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLineupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('lineups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('week');
            $table->integer('user_id')->unsigned();      
            $table->string('quarterback');
            $table->string('runningback_one');
            $table->string('runningback_two');
            $table->string('widereceiver_one');
            $table->string('widereceiver_two');
            $table->string('tightend');
            $table->string('defense');
            $table->foreign('user_id')->references('id')->on('users');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('lineups');
    }
}
