<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUser2WeeklyLineupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('user_2_weekly_lineups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('week');
            $table->string('quarterbacks');
            $table->string('runningbacks_one');
            $table->string('runningbacks_two');
            $table->string('widereceivers_one');
            $table->string('widereceivers_two');
            $table->string('tightends');
            $table->string('defense');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_2_weekly_lineups');
    }
}
