@extends('cms.layouts.master')
@section('title', 'Players')
@section('content')
 
		    <div class="container-fluid dash"><!-- container-fluid dash -->
		      <div class="row">
		      <!-- ******************   sidebar ******************************-->
		        <div class="col-sm-3 col-md-2 sidebar">
		          <ul class="nav nav-sidebar">
		            <li><a href="<?php echo LARAVEL_URL; ?>/cms">CMS </a></li>
		            <li class="active"><a href="<?php echo LARAVEL_URL; ?>/cms/players">Players</a></li>
		            <li><a href="#">Reports</a></li>
		            <li><a href="#">Analytics</a></li>
		            <li><a href="#">Export</a></li>
		          </ul>
		          <ul class="nav nav-sidebar">
		            <li><a href="<?php echo LARAVEL_URL; ?>/admin/user">Users</a></li>
		            <li><a href="">Nav item again</a></li>
		            <li><a href="">One more nav</a></li>
		            <li><a href="">Another nav item</a></li>
		            <li><a href="">More navigation</a></li>
		          </ul>
		          <ul class="nav nav-sidebar">
		            <li><a href="">Nav item again</a></li>
		            <li><a href="">One more nav</a></li>
		            <li><a href="">Another nav item</a></li>
		          </ul>
		        </div>
				<!-- ******************   sidebar ******************************-->

				<!-- ******************   content ******************************-->
		        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
		        <h1 class="page-header">@if (Auth::check()){{ Auth::user()->name }}'s Dashboard @endif</h1>
		        @if (Auth::user()->is_admin != true)
			        <h1>Access Denied! This page is only for Administrators</h1>
			    @else
		          <h1 class="page-header">Players</h1>
		          <hr>
		          <h5>Enter Players</h5>
		          
		          <form method="POST" action="">
		          	
			        <!-- error messages -->
	                @foreach ($errors->all() as $error)
	                    <p class="alert alert-danger">{{ $error }}</p>
	                @endforeach
	                <!-- status messages -->
	                @if (session('status'))
	                    <div class="alert alert-success">
	                        {{ session('status') }}
	                    </div>
	                @endif
	                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
		            <div class="form-group">
		              <label for="name">Player Name</label>
		              <input type="text" class="form-control" id="name" name="name">
		            </div>

		            <div class="form-group">
		              <label for="team">Player Team</label>
		              <select class="form-control" id="team" name="team">
			            	<option value="Arizona Cardinals">Arizona Cardinals</option>
			            	<option value="Atlanta Falcons">Atlanta Falcons</option>
			            	<option value="Baltimore Ravens">Baltimore Ravens</option>
			            	<option value="Buffalo Bills">Buffalo Bills</option>
			            	<option value="Chicago Bears">Chicago Bears</option>
			            	<option value="Cincinnati Bengals">Cincinnati Bengals</option>
			            	<option value="Cleveland Browns">Cleveland Browns</option>
			            	<option value="Dallas Cowboys">Dallas Cowboys</option>
			            	<option value="Denver Broncos">Denver Broncos</option>
			            	<option value="Detroit Lions">Detroit Lions</option>			            	
			            	<option value="Green Bay Packers">Green Bay Packers</option>
			            	<option value="Houston Texans">Houston Texans</option>
			            	<option value="Indianapolis Colts">Indianapolis Colts</option>
			            	<option value="Jacksonville Jaguars">Jacksonville Jaguars</option>
			            	<option value="Kansas City Chiefs">Kansas City Chiefs</option>
			            	<option value="Miami Dolphins">Miami Dolphins</option>
			            	<option value="Minnesota Vikings">Minnesota Vikings</option>
			            	<option value="New England Patriots">New England Patriots</option>
			            	<option value="New Orleans Saints">New Orleans Saints</option>
			            	<option value="New York Giants">New York Giants</option>		            	
			            	<option value="New York Jets">New York Jets</option>
			            	<option value="Oakland Raiders">Oakland Raiders</option>
			            	<option value="Philadelphia Eagles">Philadelphia Eagles</option>
			            	<option value="Pittsburgh Steelers">Pittsburgh Steelers</option>
			            	<option value="San Diego Chargers">San Diego Chargers</option>
			            	<option value="San Francisco 49ers">San Francisco 49ers</option>
			            	<option value="Seattle Seahawks">Seattle Seahawks</option>
			            	<option value="Los Angeles Rams">Los Angeles Rams</option>
			            	<option value="Tampa Bay Buccaneers">Tampa Bay Buccaneers</option>
			            	<option value="Tennessee Titans">Tennessee Titans</option>		            	
			            	<option value="Washington Redskins">Washington Redskins</option>
			            	<option value="Carolina">Carolina</option>

		            	</select>
		            </div>
		            <div class="form-group">
		            	<label for="position">Player Position</label>
		            	<select class="form-control" id="position" name="position">
			            	<option value="quarterback">Quarterback</option>
			            	<option value="runningback_one">Runningback One</option>
			            	<option value="runningback_two">Runningback Two</option>
			            	<option value="widereceiver_one">Widereceiver One</option>
			            	<option value="widereceiver_two">Widereceiver Two</option>
			            	<option value="tightend">Tight End</option>
			            	<option value="defense">Defense</option>
		            	</select>
		            </div>
		            <button type="submit" class="btn btn-primary">Submit</button>
		          </form>
		          
		         
		          <h2 class="sub-header">Players Section</h2>
		          <div class="row">
		          	<hr>
		          	<div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Players</h3>
                    
                        <!-- Tabs -->
                        <ul class="nav panel-tabs">
                            <li class="active"><a href="#tab1" data-toggle="tab">Quarterbacks</a></li>
                            <li><a href="#tab2" data-toggle="tab">Runningbacks One</a></li>
                            <li><a href="#tab6" data-toggle="tab">Runningbacks Two</a></li>
                            <li><a href="#tab3" data-toggle="tab">Widereceivers One</a></li>
                            <li><a href="#tab7" data-toggle="tab">Widereceivers Two</a></li>
                            <li><a href="#tab4" data-toggle="tab">Tight Ends</a></li>
                            <li><a href="#tab5" data-toggle="tab">Defense</a></li>
                        </ul>
                    
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab1"><!-- tab1 -->
									<h5>Quarterbacks</h5>
					          		<table class="table table-striped">
							            <thead>
							                <tr>
							                  <th>#</th>
							                  <th>Name</th>
							                  <th>Team</th>
							                  <th>Position</th>						            
							                  <th>Edit</th>
							                  <th>Delete</th>
							                </tr>
							            </thead>
							            <tbody>
							            @foreach($quarterbacks as $quarterback)							               							            
							                <tr>
							                  <td>{!! $quarterback->id !!}</td>
							                  <td>{!! $quarterback->name !!}</td>
							                  <td>{!! $quarterback->team !!}</td>
							                  <td>{!! ucfirst(trans($quarterback->position)) !!}</td>
							                  <td><a class="btn btn-warning" href="<?php echo LARAVEL_URL; ?>/cms/player/{!! $quarterback->id !!}/edit">Edit</a></td>		   
							                  <td>
							                  	<form method="post" action="<?php echo LARAVEL_URL; ?>/cms/player/{!! $quarterback->id !!}/delete" >
									            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
									                    <div class="form-group">
									                        <div>
									                            <button type="submit" class="btn btn-danger">Delete</button>
									                        </div>
									                    </div>
									            </form>
									          </td>
							                </tr>
							             @endforeach
							            </tbody>
						            </table>
																	
						</div><!-- tab1 -->
    					<div class="tab-pane" id="tab2"><!-- tab2 -->
								<h5>Runningback One</h5>
				          		<table class="table table-striped">
						            <thead>
						                <tr>
						                  <th>#</th>
						                  <th>Name</th>
						                  <th>Team</th>
						                  <th>Position</th>
						                </tr>
						            </thead>
						            <tbody>
						            @foreach($runningbacks_one as $runningback_one)							               							            
						                <tr>
						                  <td>{!! $runningback_one->id !!}</td>
						                  <td>{!! $runningback_one->name !!}</td>
						                  <td>{!! $runningback_one->team !!}</td>
						                  <td>{!! ucfirst(trans($runningback_one->position)) !!}</td>
						                   <td><a class="btn btn-warning" href="<?php echo LARAVEL_URL; ?>/cms/player/{!! $runningback_one->id !!}/edit">Edit</a></td>
						                   <td>
						                   	<form method="post" action="<?php echo LARAVEL_URL; ?>/cms/player/{!! $runningback_one->id !!}/delete" >
								            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
								                    <div class="form-group">
								                        <div>
								                            <button type="submit" class="btn btn-danger">Delete</button>
								                        </div>
								                    </div>
								            </form>
								           </td>		   
						                </tr>
						             @endforeach
						            </tbody>
					            </table>
							
							
						</div><!-- tab2 -->
						<div class="tab-pane" id="tab6"><!-- tab6 -->
								<h5>Runningback Two</h5>
				          		<table class="table table-striped">
						            <thead>
						                <tr>
						                  <th>#</th>
						                  <th>Name</th>
						                  <th>Team</th>
						                  <th>Position</th>
						                </tr>
						            </thead>
						            <tbody>
						            @foreach($runningbacks_two as $runningback_two)							               							            
						                <tr>
						                  <td>{!! $runningback_two->id !!}</td>
						                  <td>{!! $runningback_two->name !!}</td>
						                  <td>{!! $runningback_two->team !!}</td>
						                  <td>{!! ucfirst(trans($runningback_two->position)) !!}</td>
						                   <td><a class="btn btn-warning" href="<?php echo LARAVEL_URL; ?>/cms/player/{!! $runningback_two->id !!}/edit">Edit</a></td>
						                   <td>
						                   	<form method="post" action="<?php echo LARAVEL_URL; ?>/cms/player/{!! $runningback_two->id !!}/delete" >
								            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
								                    <div class="form-group">
								                        <div>
								                            <button type="submit" class="btn btn-danger">Delete</button>
								                        </div>
								                    </div>
								            </form>
								           </td>		   
						                </tr>
						             @endforeach
						            </tbody>
					            </table>
							
							
						</div><!-- tab6 -->
						<div class="tab-pane" id="tab3"><!-- tab3 -->
								<h5>Widereceivers One</h5>
				          		<table class="table table-striped">
						            <thead>
						                <tr>
						                  <th>#</th>
						                  <th>Name</th>
						                  <th>Team</th>
						                  <th>Position</th>
						                </tr>
						            </thead>
						            <tbody>
						               @foreach($widereceivers_one as $widereceiver_one)							               							            
						                <tr>
						                  <td>{!! $widereceiver_one->id !!}</td>
						                  <td>{!! $widereceiver_one->name !!}</td>
						                  <td>{!! $widereceiver_one->team !!}</td>
						                  <td>{!! ucfirst(trans($widereceiver_one->position)) !!}</td>
						                   <td><a class="btn btn-warning" href="<?php echo LARAVEL_URL; ?>/cms/player/{!! $widereceiver_one->id !!}/edit">Edit</a></td>
						                   <td>
						                   	<form method="post" action="<?php echo LARAVEL_URL; ?>/cms/player/{!! $widereceiver_one->id !!}/delete" >
								            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
								                    <div class="form-group">
								                        <div>
								                            <button type="submit" class="btn btn-danger">Delete</button>
								                        </div>
								                    </div>
								            </form>
						                   </td>		   
						                </tr>
						             @endforeach
						            </tbody>
					            </table>
							
							
						</div><!-- tab3 -->
						<div class="tab-pane" id="tab7"><!-- tab7 -->
								<h5>Widereceivers Two</h5>
				          		<table class="table table-striped">
						            <thead>
						                <tr>
						                  <th>#</th>
						                  <th>Name</th>
						                  <th>Team</th>
						                  <th>Position</th>
						                </tr>
						            </thead>
						            <tbody>
						               @foreach($widereceivers_two as $widereceiver_two)							               							            
						                <tr>
						                  <td>{!! $widereceiver_two->id !!}</td>
						                  <td>{!! $widereceiver_two->name !!}</td>
						                  <td>{!! $widereceiver_two->team !!}</td>
						                  <td>{!! ucfirst(trans($widereceiver_two->position)) !!}</td>
						                   <td><a class="btn btn-warning" href="<?php echo LARAVEL_URL; ?>/cms/player/{!! $widereceiver_two->id !!}/edit">Edit</a></td>
						                   <td>
						                   	<form method="post" action="<?php echo LARAVEL_URL; ?>/cms/player/{!! $widereceiver_two->id !!}/delete" >
								            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
								                    <div class="form-group">
								                        <div>
								                            <button type="submit" class="btn btn-danger">Delete</button>
								                        </div>
								                    </div>
								            </form>
						                   </td>		   
						                </tr>
						             @endforeach
						            </tbody>
					            </table>
							
							
						</div><!-- tab7 -->
						<div class="tab-pane" id="tab4"><!-- tab4 -->
								<h5>Tight Ends</h5>
				          		<table class="table table-striped">
						            <thead>
						                <tr>
						                  <th>#</th>
						                  <th>Name</th>
						                  <th>Team</th>
						                  <th>Position</th>
						                </tr>
						            </thead>
						            <tbody>
						               @foreach($tightends as $tightend)							               							            
						                <tr>
						                  <td>{!! $tightend->id !!}</td>
						                  <td>{!! $tightend->name !!}</td>
						                  <td>{!! $tightend->team !!}</td>
						                  <td>{!! ucfirst(trans($tightend->position)) !!}</td>
						                   <td><a class="btn btn-warning" href="<?php echo LARAVEL_URL; ?>/cms/player/{!! $tightend->id !!}/edit">Edit</a></td>
						                   <td>
						                   	<form method="post" action="<?php echo LARAVEL_URL; ?>/cms/player/{!! $tightend->id !!}/delete" >
								            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
								                    <div class="form-group">
								                        <div>
								                            <button type="submit" class="btn btn-danger">Delete</button>
								                        </div>
								                    </div>
								            </form>
									       </td>		   		   
						                </tr>
						             @endforeach
						            </tbody>
					            </table>	
							
							
						</div><!-- tab4 -->
						<div class="tab-pane" id="tab5"><!-- tab5 -->
								
							<h5>Defense</h5>
				          		<table class="table table-striped">
						            <thead>
						                <tr>
						                  <th>#</th>
						                  <th>Name</th>
						                  <th>Team</th>
						                  <th>Position</th>
						                </tr>
						            </thead>
						            <tbody>
						                @foreach($defenses as $defense)							               							            
						                <tr>
						                  <td>{!! $defense->id !!}</td>
						                  <td>{!! $defense->name !!}</td>
						                  <td>{!! $defense->team !!}</td>
						                  <td>{!! ucfirst(trans($defense->position)) !!}</td>
						                   <td><a class="btn btn-warning" href="<?php echo LARAVEL_URL; ?>/cms/player/{!! $defense->id !!}/edit">Edit</a></td>
						                   <td>
						                   	<form method="post" action="<?php echo LARAVEL_URL; ?>/cms/player/{!! $defense->id !!}/delete" >
								            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
								                    <div class="form-group">
								                        <div>
								                            <button type="submit" class="btn btn-danger">Delete</button>
								                        </div>
								                    </div>
								            </form>
								           </td>		   		   	   
						                </tr>
						             @endforeach
						            </tbody>
					            </table>	
							
						</div><!-- tab5 -->
                    </div>
                </div>
            </div>
		        </div>
		      </div><!-- row -->
		    </div><!-- container-fluid dash -->
		    @endif
@endsection