 
@extends('cms.layouts.master')
@section('title', 'View Users')

@section('content')

	
	@if (Auth::user()->is_admin != true)
        </h1>Access Denied! This page is only for Administrators</h1>
    @else
       
		<h1>Registered users</h1>

		<ul>
			@forelse ($users as $user)

				<li>{{ $user->name }} ({{ $user->email }})</li>

		@empty 
			
				<li>No Registered Users</li>

		@endforelse

		</ul>
	@endif
@endsection