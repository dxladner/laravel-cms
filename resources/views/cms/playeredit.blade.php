@extends('cms.layouts.master')
@section('title', 'Edit A Player')
@section('content')
 
            <div class="container-fluid dash"><!-- container-fluid dash -->
              <div class="row"><!-- row -->
              <!-- ******************   sidebar ******************************-->
                <div class="col-sm-3 col-md-2 sidebar">
                  <ul class="nav nav-sidebar">
                    <li class="active"><a href="<?php echo LARAVEL_URL; ?>/cms">CMS</a></li>
                    <li><a href="<?php echo LARAVEL_URL; ?>/cms/players">Players</a></li>
                    <li><a href="#">Reports</a></li>
                    <li><a href="#">Analytics</a></li>
                    <li><a href="#">Export</a></li>
                  </ul>
                  <ul class="nav nav-sidebar">
                    <li><a href="<?php echo LARAVEL_URL; ?>/admin/user">Users</a></li>
                    <li><a href="">Nav item again</a></li>
                    <li><a href="">One more nav</a></li>
                    <li><a href="">Another nav item</a></li>
                    <li><a href="">More navigation</a></li>
                  </ul>
                  <ul class="nav nav-sidebar">
                    <li><a href="">Nav item again</a></li>
                    <li><a href="">One more nav</a></li>
                    <li><a href="">Another nav item</a></li>
                  </ul>
                </div>
            <!-- ******************   sidebar ******************************-->

            <!-- ******************   content ******************************-->
                <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 col-md-offset-1 main">
                  <h1 class="page-header">Dashboard</h1>
                    @if (Auth::user()->is_admin != true)
                        <h1>Access Denied! This page is only for Administrators</h1>
                    @else
                        <h1 class="page-header">Edit Players</h1>
                  
                  <form method="POST" action="">
                    
                    <!-- error messages -->
                    @foreach ($errors->all() as $error)
                        <p class="alert alert-danger">{{ $error }}</p>
                    @endforeach
                    <!-- status messages -->
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                    <div class="form-group">
                      <label for="name">Player Name</label>
                      <input type="text" class="form-control" id="name" name="name" value="{!! $player->name !!}">
                    </div>

                    <div class="form-group">
                      <label for="team">Player Team</label>
                      <select class="form-control" id="team" name="team">
                            <option value="{!! $player->team !!}">{!! $player->team !!}</option>
                            <option value="Arizona Cardinals">Arizona Cardinals</option>
                            <option value="Atlanta Falcons">Atlanta Falcons</option>
                            <option value="Baltimore Ravens">Baltimore Ravens</option>
                            <option value="Buffalo Bills">Buffalo Bills</option>
                            <option value="Chicago Bears">Chicago Bears</option>
                            <option value="Cincinnati Bengals">Cincinnati Bengals</option>
                            <option value="Cleveland Browns">Cleveland Browns</option>
                            <option value="Dallas Cowboys">Dallas Cowboys</option>
                            <option value="Denver Broncos">Denver Broncos</option>
                            <option value="Detroit Lions">Detroit Lions</option>                            
                            <option value="Green Bay Packers">Green Bay Packers</option>
                            <option value="Houston Texans">Houston Texans</option>
                            <option value="Indianapolis Colts">Indianapolis Colts</option>
                            <option value="Jacksonville Jaguars">Jacksonville Jaguars</option>
                            <option value="Kansas City Chiefs">Kansas City Chiefs</option>
                            <option value="Miami Dolphins">Miami Dolphins</option>
                            <option value="Minnesota Vikings">Minnesota Vikings</option>
                            <option value="New England Patriots">New England Patriots</option>
                            <option value="New Orleans Saints">New Orleans Saints</option>
                            <option value="New York Giants">New York Giants</option>                        
                            <option value="New York Jets">New York Jets</option>
                            <option value="Oakland Raiders">Oakland Raiders</option>
                            <option value="Philadelphia Eagles">Philadelphia Eagles</option>
                            <option value="Pittsburgh Steelers">Pittsburgh Steelers</option>
                            <option value="San Diego Chargers">San Diego Chargers</option>
                            <option value="San Francisco 49ers">San Francisco 49ers</option>
                            <option value="Seattle Seahawks">Seattle Seahawks</option>
                            <option value="St. Louis Rams">St. Louis Rams</option>
                            <option value="Tampa Bay Buccaneers">Tampa Bay Buccaneers</option>
                            <option value="Tennessee Titans">Tennessee Titans</option>                      
                            <option value="Washington Redskins">Washington Redskins</option>
                            <option value="Carolina">Carolina</option>

                        </select>
                    </div>
                    <div class="form-group">
                        <label for="position">Player Position</label>
                        <select class="form-control" id="position" name="position">
                            <option value="{!! $player->position !!}">{!! $player->position !!}</option>
                            <option value="quarterback">Quarterback</option>
                            <option value="runningback_one">Runningback One</option>
                            <option value="runningback_two">Runningback Two</option>
                            <option value="widereceiver_one">Widereceiver One</option>
                            <option value="widereceiver_two">Widereceiver Two</option>
                            <option value="tightend">Tight End</option>
                            <option value="defense">Defense</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </form>

                    @endif
                    </tbody>
                    </table>
                  </div>
                </div>
              </div><!-- row -->
            </div><!-- container-fluid dash -->

@endsection