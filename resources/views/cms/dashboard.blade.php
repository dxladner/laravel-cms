@extends('cms.layouts.master')
@section('title', 'CMS Dashboard')
@section('content')
 
		    <div class="container-fluid dash"><!-- container-fluid dash -->
		      <div class="row"><!-- row -->
		       <!-- ******************   sidebar ******************************-->
		        <div class="col-sm-3 col-md-2 sidebar">
		          <ul class="nav nav-sidebar">
		          	<li class="active"><a href="<?php echo LARAVEL_URL; ?>/dashboard">Dashboard</a></li>
		             @if (Auth::user()->is_admin == true)
		            <li><a href="<?php echo LARAVEL_URL; ?>/cms">CMS</a></li>
		            @endif
		            <li><a href="<?php echo LARAVEL_URL; ?>/contestants">Players</a></li>
		            <li><a href="#">Reports</a></li>
		            <li><a href="#">Analytics</a></li>
		            <li><a href="#">Export</a></li>
		          </ul>
		          <ul class="nav nav-sidebar">
		            <li><a href="<?php echo LARAVEL_URL; ?>/admin/user">Users</a></li>
		            <li><a href="">Nav item again</a></li>
		            <li><a href="">One more nav</a></li>
		            <li><a href="">Another nav item</a></li>
		            <li><a href="">More navigation</a></li>
		          </ul>
		          <ul class="nav nav-sidebar">
		            <li><a href="">Nav item again</a></li>
		            <li><a href="">One more nav</a></li>
		            <li><a href="">Another nav item</a></li>
		          </ul>
		        </div>
		        <!-- ******************   sidebar ******************************-->

		        <!-- ******************   content ******************************-->
		        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
		          <h1 class="page-header">@if (Auth::check()){{ Auth::user()->name }}'s Dashboard @endif</h1>
		            @if (Auth::user()->is_admin == true)
			        	
		          		<h5>Create your Post</h5>
		          	
			         	<form method="post" action="{{ url('/posts') }}">
						  <input type="hidden" name="_token" value="{{ csrf_token() }}">
						  <div class="form-group">
						    <input required="required" value="{{ old('title') }}" placeholder="Enter title here" type="text" name = "title"class="form-control" />
						  </div>
						  <div class="form-group">
						    <textarea name='body'class="form-control">{{ old('body') }}</textarea>
						  </div>
						  <input type="submit" name='publish' class="btn btn-success" value = "Publish"/>
						  <input type="submit" name='save' class="btn btn-default" value = "Save Draft" />
						</form>

			          	<h2 class="sub-header">Section title</h2>
			          	<div class="table-responsive">
			            	<table class="table table-striped">
			              		<thead>
			                		<tr>
			                  			<th>ID</th>
			                  			<th>Title</th>
			                  			<th>Body</th>
			                  			<th>Created</th>
			                		</tr>
			              		</thead>
			              		<tbody>
			              		@if ($posts->isEmpty())
			                    	<p> There is no posts.</p>
			                	@else
			                    
			                    	@foreach($posts as $post)
			                    	<tr>
			                    		<td>{{ $post->id }}</td>
			                    		<td>
			                    			<a href="{!! action('PostsController@show', $post->id) !!}">    
			                            		<h5 class="header">{!! $post->title !!}</h5>
			                        		</a>
			                        	</td>	                  		
			                  			<td>{!! $post->body !!}</td>
			                  			<td>{!! $post->created_at->format('M d,Y') !!}</td>

		             				</tr>
			                    	@endforeach
			                       
			                	@endif
		               
			              		</tbody>
			            	</table>
		          		</div>
		          	@else
		          	<div class="col-md-6">
		          		<h5>Weekly Results</h5>
		          		<form id="" name="">
		          		<select class="form-control" id="weekly_results_select" name="weekly_results_select">
		          		<option value="week_one">Week One</option>
		          		</select>
			          	<div class="table-responsive">
			            	<table class="table table-striped">
			              		<thead>
			                		<tr>
			                  			<th>ID</th>
			                  			<th>Player</th>
			                  			<th>Position</th>
			                  			<th>Week</th>
			                		</tr>
			              		</thead>
			              		<tbody>
			              			<tr>
		               					<td>1</td>
		               					<td>Tony Romo</td>
		               					<td>Quarterback</td>
		               					<td>Week One</td>
		               				</tr>
			              		</tbody>
			            	</table>
			          	</div>
			          	</form>
			        </div>
			        <div class="col-md-6">
			        	<div class="table-responsive">
			            	<table class="table table-striped">
			              		<thead>
			                		<tr>
			                			<th>Place</th>
			                  			<th>Contestants</th>
			                  			<th>Week One Score</th>
			                  			<th>Overall Score</th>
			         
			                		</tr>
			              		</thead>
			              		<tbody>
			              			<tr>
			              				<td>1st</td>
		               					<td>John Doe</td>
		               					<td>112.6</td>
		               					<td>523.5</td>
		       
		               				</tr>
			              		</tbody>
			            	</table>
			          	</div>

			        </div>
		          	@endif
		        </div>
		      </div><!-- row -->
		    </div><!-- container-fluid dash -->

@endsection