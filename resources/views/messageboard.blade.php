@extends('layouts.master')
@section('title', 'Laravel Lister')
<meta id="token" name="token" value="<?php echo csrf_token(); ?>">
@section('content')
	<style>
	body {
		padding: 2em 0;
	}
	.error {
	font-weight: bold;
	color: red;
}
	</style>
        <div class="content" id="messageboard">
            <div class="title">Message Board</div>
            
            <form method="post" v-on="submit: onSubmitForm">

            	<div class="form-group">
            		<label for="name">Name: <span class="error" v-if="! newMessage.name">Name is required</span></label>
            		<input type="text" name="name" id="name" class="form-control" v-model="newMessage.name">
            	</div>

            	<div class="form-group">
            		<label for="message">Message: <span class="error" v-if="! newMessage.message">Name is required</span></label>
            		<textarea type="text" name="message" id="message" class="form-control" v-model="newMessage.message">
            		</textarea>
            	</div>

            	<div class="form-group" v-if="! submitted">
					<button type="submit" class="btn btn-primary" v-attr="disabled: errors">Post Message</button>
				</div>

            </form>
			<div class="alert alert-success" v-show="submitted">Thanks!</div>
            <hr>

            <div>

	            <article v-repeat="messages">
					<h3>@{{ name }}</h3>

					<div class="body">
						@{{ message }}
					</div>

				</article>

			<!-- 	<pre>@{{ $data | json }}</pre> -->
            </div>
        </div>

@endsection
