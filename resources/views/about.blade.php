@extends('layouts.master')
@section('title', 'About')

@section('content')
  
        <div class="content">
            <div class="title">About: Laravel Lister Landing </div>
            <h5>What is Laravel Lister?</h5>
            <p>
                Laravel Lister Landing Page for the ultimate productivity application for
                tropically-minded users built in the BEST PHP Framework.
            </p>
        </div>
   
@endsection
