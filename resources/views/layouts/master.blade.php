<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{!! asset('css/public-styles.css') !!}" >
    <link rel="stylesheet" type="text/css" href="{!! asset('css/flat-ui.min.css') !!}" >

</head>

<body>
    @extends('layouts.navbar')

    <div class="container">

        @yield('content')

    </div>

    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="{!! asset('js/admin.js') !!}"></script>
    <script src="{!! asset('js/bootstrap-datetimepicker.js') !!}"></script>
    <script src="{!! asset('js/chosen.min.js') !!}"></script>
    <!-- // <script src="{!! asset('js/datetimepicker.js') !!}"></script> -->
    <script src="{!! asset('js/flat-ui.min.js') !!}"></script>
    <script src="{!! asset('js/respond.min.js') !!}"></script>
    <script src="{!! asset('js/video.js') !!}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/0.11.10/vue.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/0.7.0/vue-resource.js"></script>
    <script src="{!! asset('js/messageboard.js') !!}"></script>
    <script type="text/javascript" src="{{ asset('/js/tinymce/tinymce.min.js') }}"></script>
    <script type="text/javascript">
      tinymce.init({
        selector : "textarea",
        plugins : ["advlist autolink lists link image charmap print preview anchor", "searchreplace visualblocks code fullscreen", "insertdatetime media table contextmenu paste"],
        toolbar : "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
      }); 
    </script>
</body>
</html>
