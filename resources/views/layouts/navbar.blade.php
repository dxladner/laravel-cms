<!-- Fixed navbar -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
         <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo LARAVEL_URL; ?>">LARAVEL CMS</a>
           
            @if (Auth::check())
                <span class="username">{{ Auth::user()->name }}</span>
                          <b class="caret"></b>
                        @else
                           <p>Please login!</p>
                        @endif
                        
        </div>
        <!-- Navbar Right -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li class="active"><a href="<?php echo LARAVEL_URL; ?>">Home</a></li>
                <li><a href="<?php echo LARAVEL_URL; ?>/welcome">Welcome</a></li>
                <li><a href="<?php echo LARAVEL_URL; ?>/posts">Blog</a></li>
                <li><a href="<?php echo LARAVEL_URL; ?>/about">About</a></li>
                <li><a href="<?php echo LARAVEL_URL; ?>/contact">Contact</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Tickets <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="<?php echo LARAVEL_URL; ?>/create">Create Ticket</a></li>
                        <li><a href="<?php echo LARAVEL_URL; ?>/tickets">View Tickets</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Member <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        @if (Auth::check())
                            @if (Auth::user()->is_admin == true)
                            <li><a href="<?php echo LARAVEL_URL; ?>/cms">CMS</a></li>
                            @endif

                            <li><a href="<?php echo LARAVEL_URL; ?>/users/logout">Logout</a></li>
                        @else
                            <li><a href="<?php echo LARAVEL_URL; ?>/users/register">Register</a></li>
                            <li><a href="<?php echo LARAVEL_URL; ?>/users/login">Login</a></li>
                        @endif
                    </ul>
                </li>
            </ul>
        </div>
      </div>
    </nav>