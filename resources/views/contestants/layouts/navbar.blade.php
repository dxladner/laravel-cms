<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">LARAVEL CMS</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="<?php echo LARAVEL_URL; ?>/cms">Dashboard</a></li>
             <li><a href="<?php echo LARAVEL_URL; ?>/home">Site</a></li>
            <li><a href="#">Settings</a></li>
            
            <li><a href="#">Help</a></li>
            <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Member <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        @if (Auth::check())
                            @if (Auth::user()->is_admin == true)
                            <li><a href="<?php echo LARAVEL_URL; ?>/cms">CMS</a></li>
                            @endif
                            <li><a href="<?php echo LARAVEL_URL; ?>/admin/user/{{ Auth::user()->id }}">Profile</a></li>
                            <li><a href="<?php echo LARAVEL_URL; ?>/users/logout">Logout</a></li>
                        @else
                            <li><a href="<?php echo LARAVEL_URL; ?>/users/register">Register</a></li>
                            <li><a href="<?php echo LARAVEL_URL; ?>/users/login">Login</a></li>
                        @endif
                    </ul>
                </li>
          </ul>
          <form class="navbar-form navbar-right">
            <input type="text" class="form-control" placeholder="Search...">
          </form>
        </div>
      </div>
    </nav>
