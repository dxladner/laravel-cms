@extends('cms.layouts.master')
@section('title', 'CMS Dashboard')
@section('content')
 
		<div class="container-fluid dash"><!-- container-fluid dash -->
		    <div class="row"><!-- row -->
		       <!-- ******************   sidebar ******************************-->
		        <div class="col-sm-3 col-md-2 sidebar">
		          <ul class="nav nav-sidebar">
		            <li class="active"><a href="<?php echo LARAVEL_URL; ?>/dashboard">Dashboard</a></li>
		            @if (Auth::user()->is_admin == true)
		            <li><a href="<?php echo LARAVEL_URL; ?>/cms">CMS</a></li>
		            <li><a href="<?php echo LARAVEL_URL; ?>/cms/players"> CMS Players</a></li>
		            @endif
		            <li><a href="<?php echo LARAVEL_URL; ?>/contestants">Players</a></li>
		            <li><a href="#">Reports</a></li>
		            <li><a href="#">Analytics</a></li>
		            <li><a href="#">Export</a></li>
		          </ul>
		          <ul class="nav nav-sidebar">
		            <li><a href="<?php echo LARAVEL_URL; ?>/admin/user">Users</a></li>
		            <li><a href="">Nav item again</a></li>
		            <li><a href="">One more nav</a></li>
		            <li><a href="">Another nav item</a></li>
		            <li><a href="">More navigation</a></li>
		          </ul>
		          <ul class="nav nav-sidebar">
		            <li><a href="">Nav item again</a></li>
		            <li><a href="">One more nav</a></li>
		            <li><a href="">Another nav item</a></li>
		          </ul>
		        </div>
		        <!-- ******************   sidebar ******************************-->

		        <!-- ******************   content ******************************-->
		        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
		          <h1 class="page-header">@if (Auth::check()){{ Auth::user()->name }}'s Dashboard @endif</h1>
		          <!-- error messages -->
	                @foreach ($errors->all() as $error)
	                    <p class="alert alert-danger">{{ $error }}</p>
	                @endforeach
	                <!-- status messages -->
	                @if (session('status'))
	                    <div class="alert alert-success">
	                        {{ session('status') }}
	                    </div>
	                @endif
		           <h5>Week One</h5>
		           <P>Please enter your players for Week One</p>
		           <form id="weekly_entry" method="post" action="">
		           		 @foreach ($errors->all() as $error)
                    		<p class="alert alert-danger">{{ $error }}</p>
                		@endforeach

                		<input type="hidden" name="_token" value="{!! csrf_token() !!}">
                		<input type="hidden" name="week" value="week_one">
                		<input type="hidden" name="user_id" value="{{ Auth::user()->id }}">

                		<fieldset>
	                    <legend>Pick Players</legend>
	                    <div class="form-group">
	                        <label for="title" class="col-md-3 control-label">Quarterback</label>
	                        <div class="col-md-9">
	                        	<select class="form-control" id="quarterback" name="quarterback">
	                        		@foreach($quarterbacks as $quarterback)

			                   			<option value="{{ $quarterback->name }}">{{ $quarterback->name }}</option>
			                  		
			                    	@endforeach
				            	</select>
	                        </div>
	                    </div>
	                    <br><br>
	                    <div class="form-group">
	                        <label for="content" class="col-md-3 control-label">Runningback One</label>
	                        <div class="col-md-9">
	                           <select class="form-control" id="runningback_one" name="runningback_one">
	                        		@foreach($runningbacks_one as $runningback_one)

			                   			<option value="{{ $runningback_one->name }}">{{ $runningback_one->name }}</option>
			                  		
			                    	@endforeach
				            	</select>
	                        </div>
	                    </div>
	                    <br><br>
	                    <div class="form-group">
	                        <label for="content" class="col-md-3 control-label">Runningback Two</label>
	                        <div class="col-md-9">
	                            <select class="form-control" id="runningback_two" name="runningback_two">
	                        		@foreach($runningbacks_two as $runningback_two)

			                   			<option value="{{ $runningback_two->name }}">{{ $runningback_two->name }}</option>
			                  		
			                    	@endforeach
				            	</select>
	                        </div>
	                    </div>
	                    <br><br>
	                    <div class="form-group">
	                        <label for="content" class="col-md-3 control-label">Wide Receiver One</label>
	                        <div class="col-lg-9">
	                            <select class="form-control" id="widereceiver_one" name="widereceiver_one">
	                        		@foreach($widereceivers_one as $widereceiver_one)

			                   			<option value="{{ $widereceiver_one->name }}">{{ $widereceiver_one->name }}</option>
			                  		
			                    	@endforeach
				            	</select>
	                        </div>
	                    </div>
	                    <br><br>
	                    <div class="form-group">
	                        <label for="content" class="col-md-3 control-label">Wide Receiver Two</label>
	                        <div class="col-md-9">
	                            <select class="form-control" id="widereceiver_two" name="widereceiver_two">
	                        		@foreach($widereceivers_two as $widereceiver_two)

			                   			<option value="{{ $widereceiver_two->name }}">{{ $widereceiver_two->name }}</option>
			                  		
			                    	@endforeach
				            	</select>
	                        </div>
	                    </div>
	                    <br><br>
	                    <div class="form-group">
	                        <label for="content" class="col-md-3 control-label">Tight End</label>
	                        <div class="col-md-9">
	                            <select class="form-control" id="tightend" name="tightend">
	                        		@foreach($tightends as $tightend)

			                   			<option value="{{ $tightend->name }}">{{ $tightend->name }}</option>
			                  		
			                    	@endforeach
				            	</select>
	                        </div>
	                    </div>
	                    <br><br>
	                    <div class="form-group">
	                        <label for="content" class="col-md-3 control-label">Defense</label>
	                        <div class="col-md-9">
	                            <select class="form-control" id="defense" name="defense">
	                        		@foreach($defenses as $defense)

			                   			<option value="{{ $defense->name }}">{{ $defense->name }}</option>
			                  		
			                    	@endforeach
				            	</select>
	                        </div>
	                    </div>
	                    <br><br>
	                    <div class="form-group">
	                        <div class="col-md-12">   
	                            <button type="submit" class="btn btn-primary">Save</button>
	                        </div>
	                    </div>
	                </fieldset>
		           </form>
		        
			        <hr>
			        <h3>Weekly Lineup</h3>
			        <div class="table-responsive">
		            	<table class="table table-striped">
		              		<thead>
		                		<tr>
		                  			<th>ID</th>
		                  			<th>Player</th>
		                  			<th>Position</th>
		                  			<th>Week</th>
		                		</tr>
		              		</thead>
		              		<tbody>
		              		
		              		</tbody>
		            	</table>
			        </div>
			    </div>
		    </div><!-- row -->
		</div><!-- container-fluid dash -->

@endsection