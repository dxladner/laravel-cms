@extends('cms.layouts.master')
@section('title', 'CMS Dashboard')
@section('content')
 
		    <div class="container-fluid dash"><!-- container-fluid dash -->
		      <div class="row"><!-- row -->
		      <!-- ******************   sidebar ******************************-->
		        <div class="col-sm-3 col-md-2 sidebar">
		          <ul class="nav nav-sidebar">
		            <li class="active"><a href="<?php echo LARAVEL_URL; ?>/cms">CMS</a></li>
		            <li><a href="<?php echo LARAVEL_URL; ?>/cms/players">Players</a></li>
		            <li><a href="#">Reports</a></li>
		            <li><a href="#">Analytics</a></li>
		            <li><a href="#">Export</a></li>
		          </ul>
		          <ul class="nav nav-sidebar">
		            <li><a href="<?php echo LARAVEL_URL; ?>/admin/user">Users</a></li>
		            <li><a href="">Nav item again</a></li>
		            <li><a href="">One more nav</a></li>
		            <li><a href="">Another nav item</a></li>
		            <li><a href="">More navigation</a></li>
		          </ul>
		          <ul class="nav nav-sidebar">
		            <li><a href="">Nav item again</a></li>
		            <li><a href="">One more nav</a></li>
		            <li><a href="">Another nav item</a></li>
		          </ul>
		        </div>
		    <!-- ******************   sidebar ******************************-->

		    <!-- ******************   content ******************************-->
		        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 col-md-offset-1 main">
		          <h1 class="page-header">@if (Auth::check()){{ Auth::user()->name }}'s Dashboard @endif</h1>
		            @if (Auth::user()->is_admin != true)
				        <h1>Access Denied! This page is only for Administrators</h1>
				    @else
		            <h5>User Admin</h5>

						<h1>Registered users</h1>
						<div class="table-responsive">
				            <table class="table table-striped">
				              <thead>
				                <tr>
				                  <th>#</th>
				                  <th>Name</th>
				                  <th>Email</th>
				                </tr>
				              </thead>
				              <tbody>
								<ul>
							@forelse ($users as $user)
								<tr>
								<td>{{ $user->id }}</td>
								<td>{{ $user->name }}</td>
		                  		<td>{{ $user->email }}</td>
								</tr>
						@empty 
							<tr>
								<td colspan="2">No Registered Users</td>
							</tr>
						@endforelse

					@endif
					</tbody>
		            </table>
		          </div>
		        </div>
		      </div><!-- row -->
		    </div><!-- container-fluid dash -->

@endsection