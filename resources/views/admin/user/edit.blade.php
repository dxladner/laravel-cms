@extends('cms.layouts.master')
@section('title', 'Edit your Profile')

@section('content')
      <div class="container-fluid dash"><!-- container-fluid dash -->
              <div class="row">
              <!-- ******************   sidebar ******************************-->
                <div class="col-sm-3 col-md-2 sidebar">
                  <ul class="nav nav-sidebar">
                    <li><a href="<?php echo LARAVEL_URL; ?>/cms">CMS</a></li>
                    <li class="active"><a href="<?php echo LARAVEL_URL; ?>/cms/players">Players</a></li>
                    <li><a href="#">Reports</a></li>
                    <li><a href="#">Analytics</a></li>
                    <li><a href="#">Export</a></li>
                  </ul>
                  <ul class="nav nav-sidebar">
                    <li><a href="<?php echo LARAVEL_URL; ?>/admin/user">Users</a></li>
                    <li><a href="">Nav item again</a></li>
                    <li><a href="">One more nav</a></li>
                    <li><a href="">Another nav item</a></li>
                    <li><a href="">More navigation</a></li>
                  </ul>
                  <ul class="nav nav-sidebar">
                    <li><a href="">Nav item again</a></li>
                    <li><a href="">One more nav</a></li>
                    <li><a href="">Another nav item</a></li>
                  </ul>
                </div>
                <!-- ******************   sidebar ******************************-->

                <!-- ******************   content ******************************-->
                <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                  <h1 class="page-header">@if (Auth::check()){{ Auth::user()->name }}'s Profile @endif</h1>
                  <hr>
                  <h5>Profile</h5>
               
                    <!-- error messages -->
                    @foreach ($errors->all() as $error)
                        <p class="alert alert-danger">{{ $error }}</p>
                    @endforeach
                    <!-- status messages -->
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                       <div class="well well bs-component">

            <form method="POST" class="form-horizontal">
                @foreach ($errors->all() as $error)
                    <p class="alert alert-danger">{{ $error }}</p>
                @endforeach

                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
<input name="_method" type="hidden" value="PATCH">
                <fieldset>
                    <legend>Edit your Profile</legend>
                    <div class="form-group">
                        <label for="name" class="col-lg-2 control-label">Username</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" id="name" name="name" value="{!! $user->name !!}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-lg-2 control-label">Email</label>
                        <div class="col-lg-10">
                          <input type="text" class="form-control" id="email" name="email" value="{!! $user->email !!}">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-10 col-lg-offset-2">
                            <button class="btn btn-default">Cancel</button>
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>

                
                </div>
              </div><!-- row -->
            </div><!-- container-fluid dash -->
@endsection