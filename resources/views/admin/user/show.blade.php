@extends('cms.layouts.master')
@section('title', 'Players')
@section('content')
 
		    <div class="container-fluid dash"><!-- container-fluid dash -->
		      <div class="row">
		      <!-- ******************   sidebar ******************************-->
		        <div class="col-sm-3 col-md-2 sidebar">
		          <ul class="nav nav-sidebar">
		            <li><a href="<?php echo LARAVEL_URL; ?>/cms">CMS </a></li>
		            <li class="active"><a href="<?php echo LARAVEL_URL; ?>/cms/players">Players</a></li>
		            <li><a href="#">Reports</a></li>
		            <li><a href="#">Analytics</a></li>
		            <li><a href="#">Export</a></li>
		          </ul>
		          <ul class="nav nav-sidebar">
		            <li><a href="<?php echo LARAVEL_URL; ?>/admin/user">Users</a></li>
		            <li><a href="">Nav item again</a></li>
		            <li><a href="">One more nav</a></li>
		            <li><a href="">Another nav item</a></li>
		            <li><a href="">More navigation</a></li>
		          </ul>
		          <ul class="nav nav-sidebar">
		            <li><a href="">Nav item again</a></li>
		            <li><a href="">One more nav</a></li>
		            <li><a href="">Another nav item</a></li>
		          </ul>
		        </div>
				<!-- ******************   sidebar ******************************-->

				<!-- ******************   content ******************************-->
		        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
		          <h1 class="page-header">@if (Auth::check()){{ Auth::user()->name }}'s Profile @endif</h1>
		          <hr>
		          <h5>Profile</h5>
		       
			        <!-- error messages -->
	                @foreach ($errors->all() as $error)
	                    <p class="alert alert-danger">{{ $error }}</p>
	                @endforeach
	                <!-- status messages -->
	                @if (session('status'))
	                    <div class="alert alert-success">
	                        {{ session('status') }}
	                    </div>
	                @endif
	              	
		          		<table class="table table-striped">
				            <thead>
				                <tr>
				                  <th>#</th>
				                  <th>Username</th>
				                  <th>Email</th>
				                  <th>Password</th>
				                  <th>Edit</th>
				                </tr>
				            </thead>
				            <tbody>
				         			               							            
				                <tr>
				                  <td>{!! $user->id !!}</td>
				                  <td>{!! $user->name !!}</td>
				                  <td>{!! $user->email !!}</td>
				                  <td>{!! $user->password !!}</td>
				                  <td><a class="btn btn-warning" href="<?php echo LARAVEL_URL; ?>/admin/user/{!! $user->id !!}/edit">Edit</a></td>		   
				                </tr>
				            
				            </tbody>
			            </table>
		         

		        
		        </div>
		      </div><!-- row -->
		    </div><!-- container-fluid dash -->
		    
@endsection