@extends('layouts.master')
@section('title', 'Blog')

@section('content')
  	
    <div class="container col-md-8 col-md-offset-2">
        <div class="well well bs-component">
            <div class="content">

                @if ($posts->isEmpty())
                    <p> There is no posts.</p>
                @else
                    
                    @foreach($posts as $post)
                        <a href="{!! action('PostsController@show', $post->id) !!}">    
                            <h3 class="header">{!! $post->title !!}</h3>
                        </a>
                        
                        <p> {!! $post->body !!} </p>
                    @endforeach
                       
                @endif
            </div>
        </div>
    </div>
   
@endsection
