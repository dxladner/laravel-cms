@extends('layouts.master')
@section('title', 'Blog Posts')

@section('content')
    
    <div class="container col-md-8 col-md-offset-2">
        <div class="well well bs-component">
            <div class="content">
                <h2 class="header">{!! $post->title !!}</h2>
                <p> {!! $post->body !!} </p>
            </div>

            @if (Auth::check())
            <table>
                <tr>
                    <td>
                        <a href="{!! action('PostsController@index') !!}" class="btn btn-primary">Cancel</a>
                    </td>
                    <td>
                    &nbsp;
                    </td>
                    <td>
                        <a href="{{ URL::to('/posts/edit/' . $post->id) }}" class="btn btn-warning">Edit Post</a>
                    </td>
                    <td>&nbsp;
                    </td>
                    <td>
                        <form id="deleteForm" method="post" action="{!! action('PostsController@destroy', $post->id) !!}" >
                            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                            <div class="form-group">
                                <div>
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </div>
                            </div>
                        </form>
                    </td>
                </tr>
            </table>     
            @endif
            <div class="clearfix"></div>        
        </div>

    </div>

   
@endsection
