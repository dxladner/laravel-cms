<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Contact</title>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{!! asset('css/public-styles.css') !!}" >
    <link rel="stylesheet" type="text/css" href="{!! asset('css/flat-ui.min.css') !!}" >

</head>

<body>


  	<!-- Fixed navbar -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
         <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo LARAVEL_URL; ?>">LARAVEL CMS</a>
           
            @if (Auth::check())
                <span class="username">{{ Auth::user()->name }}</span>
                          <b class="caret"></b>
                        @else
                           <p>Please login!</p>
                        @endif
                        
        </div>
        <!-- Navbar Right -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li class="active"><a href="<?php echo LARAVEL_URL; ?>">Home</a></li>
                <li><a href="<?php echo LARAVEL_URL; ?>/welcome">Welcome</a></li>
                <li><a href="<?php echo LARAVEL_URL; ?>/posts">Blog</a></li>
                <li><a href="<?php echo LARAVEL_URL; ?>/about">About</a></li>
                <li><a href="<?php echo LARAVEL_URL; ?>/contact">Contact</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Tickets <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="<?php echo LARAVEL_URL; ?>/create">Create Ticket</a></li>
                        <li><a href="<?php echo LARAVEL_URL; ?>/tickets">View Tickets</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Member <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        @if (Auth::check())
                            <li><a href="<?php echo LARAVEL_URL; ?>/cms">CMS</a></li>
                            <li><a href="<?php echo LARAVEL_URL; ?>/users/logout">Logout</a></li>
                        @else
                            <li><a href="<?php echo LARAVEL_URL; ?>/users/register">Register</a></li>
                            <li><a href="<?php echo LARAVEL_URL; ?>/users/login">Login</a></li>
                        @endif
                    </ul>
                </li>
            </ul>
        </div>
      </div>
    </nav>
    
    <div class="container">

            <div class="title">Contact: Survivor Football</div>
            <h5>Ready to Contact Survivor Football?</h5>
            <form id="contactForm" class="form-horizontal" method="post">
			  <input type="hidden" name="_token" value="{{ csrf_token() }}">
			  	
	                <div class="form-group">
	                    <label for="title" class="col-lg-2 control-label">Name</label>
	                    <div class="col-lg-10">
	                    	<input required="required" type="text" id="name" name="name" class="form-control" />
	                    </div>
	                </div>
	           
	                <div class="form-group">
	                    <label for="title" class="col-lg-2 control-label">Email</label>
	                    <div class="col-lg-10">
	                    	<input required="required" type="text" id="email" name="email" class="form-control" />
	                    </div>
	                </div>
	            
	                <div class="form-group">
	                    <label for="title" class="col-lg-2 control-label">Message Subject</label>
	                    <div class="col-lg-10">
	                    	<select class="form-control" id="message_subject" name="message_subject">
			                   	<option value="Technical Support">Technical Support</option>
			                  	<option value="Suggestions">Suggestions</option>
			                  	<option value="Just To Say Hey">Just To Say Hey!</option>
			                  	<option value="Other">Other</option>
				            </select>
	                    </div>
	                </div>
	           
	                <div class="form-group">
	                    <label for="title" class="col-lg-2 control-label">Message</label>
	                    <div class="col-lg-10">
	                    	<textarea id="message" name="message" class="form-control"></textarea>
	                    </div>
	                </div>
	            
	            	 <div class="form-group">
                        <div class="col-lg-10 col-lg-offset-2">   
                            <button type="submit" class="btn btn-primary">Send</button>
                        </div>
                    </div>
	            
			</form>
        
   </div>

    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="{!! asset('js/admin.js') !!}"></script>
    <script src="{!! asset('js/bootstrap-datetimepicker.js') !!}"></script>
    <script src="{!! asset('js/chosen.min.js') !!}"></script>
    <!-- // <script src="{!! asset('js/datetimepicker.js') !!}"></script> -->
    <script src="{!! asset('js/flat-ui.min.js') !!}"></script>
    <script src="{!! asset('js/respond.min.js') !!}"></script>
    <script src="{!! asset('js/video.js') !!}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/0.11.10/vue.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/0.7.0/vue-resource.js"></script>
    <script src="{!! asset('js/messageboard.js') !!}"></script>
</body>
</html>