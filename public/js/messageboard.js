Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('value');

new Vue({

	el: '#messageboard',

	data: {
		newMessage: {
			name: '',
			message: ''
		},

		submitted: false,
	},

	computed: {
		errors: function() {
			
			for ( var key in this.newMessage) {
				if ( ! this.newMessage[key]) return true;
			}

			return false;
		}
	}, 

	ready: function() {
		
		this.fetchMessages();
	},

	methods: {

		fetchMessages: function() {
			
			this.$http.get('http://localhost/learnlaravel/public/api/messages', function(messages) {
				this.$set('messages', messages);
			});
		},

		onSubmitForm: function(e) {
			// prevent the default action
			e.preventDefault();
			
			var message = this.newMessage;

			// add new message to 'messages' array
			this.messages.push(this.newMessage);

			// reset input values
			this.newMessage = { name: '', message: '' };

			// show success message 
			this.submitted = true

			// send POST AJAX request
			this.$http.post('http://localhost/learnlaravel/public/api/messages', message);

		}
	}

});